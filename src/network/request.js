import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://123.207.32.32:8000/api/v1',
    timeout: 5000
})

// axios请求拦截器
instance.interceptors.request.use(
    // 请求成功拦截
    config => {
        // console.log(config)        
        return config
    },
    // 请求失败拦截
    err => {
        // console.log(err)
        return err
    }
)

// axios响应拦截器
instance.interceptors.response.use(
    // 响应成功拦截
    res => {
        // console.log(res)
        // return res.data
        return res
    },
    // 响应失败拦截
    err => {
        // console.log(err)
        return err
    }
)

export default function(config) {
    return instance(config)  // axios和axios都是Promise的子类, 可以直接返回
}